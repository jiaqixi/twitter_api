I build a web application with a simple HTTP endpoint http://localhost:3000/histogram/<name> that returns the number of tweets by <name> per hour of the day.

For example,
http:// localhost: 3000/histogram/dhh   would display the counts per hour of tweets from @dhh for the last 24 hours.

I use Twitter API with Node.js and Express to implement this function. I use Behaviour-Driven Development(BDD) to write the unit test with Mocha and Chai.




